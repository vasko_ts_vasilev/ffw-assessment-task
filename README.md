# README #

# How To Get The Backend App Running #

* Perform `npm i -g @nestjs/cli` globally
* Perform `npm install --save @nestjs/typeorm typeorm pg` in the './backend-api' folder because 
typeorm needs to be additionally installed.

* If you use the exported DB rename the `example-env` file to `.env`. The credentials
are prefilled there, otherwise create your own but a new DB will be needed.
* Use `nest start` to start the dev server and you're ready to go!

# DB registered users #

* `test_1@mail.bg`, password: `admin123`;
* `test_2@mail.bg`, password: `admin123`;
* `test_3@mail.bg`, password: `admin123`;

# How To Get The Frontend App Running #

* Perform `npm install` command in the './frontend-api' folder
* Run `npm start` and..
* That's it!

# Facebook Sharing Option #

* The credentials are as follows:
    - `email`: `vasko.ts.vasilev@gmail.com`
    - `password`: `admin123` 
    /* The email is real but the account is still only for this project */ 

* The App is already linked, however the domain must be changed to a secure one
    - I used `ngrok` -> `https://ngrok.com/`
        - after registration in their website download the tool for your Operation System
        and follow their startup process
        - then copy the secure (`https`) link and paste it in ->
            -> FB Dashboard -> Settings -> Basic -> in the `App Domains` field ->
            -> paste it at the bottom of the page in the `Site URL` field -> Save Changes

    - feel free to use any other such tool