import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';

import { User } from 'src/users/entities/user.entity';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { SignInDto } from './dto/sign-in.dto';
import { JwtService } from '@nestjs/jwt';
import { AuthResponseDto } from './dto/auth-response.dto';

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
        private jwtService: JwtService
    ) { }

    async signUp(createUserDto: CreateUserDto): Promise<AuthResponseDto> {
        const { firstName, lastName, email, password } = createUserDto;

        const salt = await bcrypt.genSalt();
        const hashedPwd = await bcrypt.hash(password, salt)

        const user = this.userRepository.create({
            firstName,
            lastName,
            email,
            password: hashedPwd
        });

        await this.userRepository.save(user);
        /**TODO
         * remove password from user
         */
        const payload = { email };
        const accessToken: string = await this.jwtService.sign(payload);

        return  { token: accessToken, user };
    }

    async signIn(signInDto: SignInDto): Promise<AuthResponseDto> {
        const { email, password } = signInDto;
        const user = await this.userRepository.findOne({ email });

        if (!user || !await bcrypt.compare(password, user.password)) {
            throw new UnauthorizedException('Please check your credentials');
        }

        const payload = { email };
        const accessToken: string = await this.jwtService.sign(payload);

        return { token: accessToken, user };
    }
    // TODO finish the sign out
    signOut(id: string) {
        return id;
    }
}
