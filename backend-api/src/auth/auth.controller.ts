import { Body, Controller, Delete, Param, Post } from '@nestjs/common';

import { AuthService } from './auth.service';
import { User } from 'src/users/entities/user.entity';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { SignInDto } from './dto/sign-in.dto';
import { AuthResponseDto } from './dto/auth-response.dto';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/sign-up')
  signUp(@Body() createUserDto: CreateUserDto): Promise<AuthResponseDto> {
    return this.authService.signUp(createUserDto);
  }

  @Post('/sign-in')
  signIn(@Body() signInDto: SignInDto): Promise<AuthResponseDto> {
    return this.authService.signIn(signInDto);
  }
  
  @Post('/sign-out/:id')
  async signOut(@Param('id') id: number): Promise<void> {
    return null;
  }
}
