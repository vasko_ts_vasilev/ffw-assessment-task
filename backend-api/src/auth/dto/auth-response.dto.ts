import { User } from "src/users/entities/user.entity";

export class AuthResponseDto {
    user: User
    token: string
}