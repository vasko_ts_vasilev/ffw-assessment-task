import { NestFactory } from '@nestjs/core';
import * as express from 'express';
import { join } from 'path';

import { AppModule } from './app.module';
import { TransformInterceptor } from './auth/transform.interceptor';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.use('/uploads', express.static(join(process.cwd(), '/uploads')));
  app.useGlobalInterceptors(new TransformInterceptor);
  await app.listen(8000);
}
bootstrap();
