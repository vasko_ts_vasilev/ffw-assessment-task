import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';

import { User } from 'src/users/entities/user.entity';

@Entity()
export class Banner {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column()
    imagePath: string;

    @ManyToOne(() => User, user=> user.banners, { eager: false })
    user: User
}
