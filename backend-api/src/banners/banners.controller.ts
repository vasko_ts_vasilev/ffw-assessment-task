import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UploadedFile,
  UseInterceptors,
  UseGuards,
  Req,
  Query,
  Optional,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import { Express } from 'express';

import { GetUser } from 'src/auth/get-user.decorator';
import { User } from 'src/users/entities/user.entity';
import { BannersService } from './banners.service';
import { CreateBannerDto } from './dto/create-banner.dto';
import { UpdateBannerDto } from './dto/update-banner.dto';
import { saveImageToStorage } from './helpers/image-storage';

@Controller('banners')
export class BannersController {
  constructor(private readonly bannersService: BannersService) { }

  @Post()
  @UseGuards(AuthGuard())
  create(
    @Body() createBannerDto: CreateBannerDto,
    @GetUser() user: User
  ) {
    return this.bannersService.create(createBannerDto, user);
  }

  @Get()
  findAll(@Query('userId') userId: string) {
    return this.bannersService.findAll(userId);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.bannersService.findOne(+id);
  }

  @Patch(':id')
  @UseGuards(AuthGuard())
  update(@Param('id') id: string, @Body() updateBannerDto: UpdateBannerDto) {
    return this.bannersService.update(+id, updateBannerDto);
  }

  @Delete(':id')
  @UseGuards(AuthGuard())
  remove(
    @Param('id') id: string,
    @GetUser() user: User
  ) {
    return this.bannersService.remove(+id, user);
  }

  @Post('/:id')
  @UseGuards(AuthGuard())
  @UseInterceptors(FileInterceptor('file', saveImageToStorage))
  uploadFile(
    @UploadedFile() file: Express.Multer.File,
    @Param('id') id: string,
    @GetUser() user: User
  ) {
    return this.bannersService.uploadFile(file, id, user);
  }
}
