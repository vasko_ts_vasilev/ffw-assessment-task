import { diskStorage } from 'multer';
import * as fs from 'fs';
import { v4 as uuidv4 } from 'uuid';
import { BadRequestException } from '@nestjs/common';

import path = require('path');

type validMimeType = 'image/png' | 'image/jpg' | 'image/jpeg';

const validFileExtensions = /\.(jpg|jpeg|png)$/;
const validMimeTypes: validMimeType[] = ['image/png', 'image/jpg', 'image/jpeg'];

export class StorageHandler {
    static customDestination(req, _file, cb) {
        const destinationFolder = `./uploads/${req.query.userId}`;

        if (!fs.existsSync(destinationFolder)) {
            fs.mkdirSync(destinationFolder, { recursive: true });
        }

        cb(null, destinationFolder);
    }

    static customFileName(req, file, cb) {
        const fileExtention: string = path.extname(file.originalname);
        const fileName: string = uuidv4() + fileExtention;

        cb(null, fileName);
    }

    static customFileFilter(_req, file, cb) {
        const allowedMimeTypes: validMimeType[] = validMimeTypes;

        if (!allowedMimeTypes.includes(file.mimetype) || !file.originalname.match(validFileExtensions)) {
            cb(null, false);
        }

        cb(null, true);
    }

    static removeFile(fullFilePath: string): void {
        
        try {
            fs.unlinkSync(fullFilePath);
        } catch (err) {
            throw new BadRequestException('File removal failed');
        }
    }
}

export const saveImageToStorage = {
    storage: diskStorage({
        destination: StorageHandler.customDestination, 
        filename: StorageHandler.customFileName
    }),
    fileFilter: StorageHandler.customFileFilter,
    limits: { fileSize: 1000000 }
};