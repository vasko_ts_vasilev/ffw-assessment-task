import { BadRequestException, Injectable, NotFoundException, UnsupportedMediaTypeException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { User } from 'src/users/entities/user.entity';
import { CreateBannerDto } from './dto/create-banner.dto';
import { UpdateBannerDto } from './dto/update-banner.dto';
import { ResponseBannerDto } from './dto/response-banner.dto'; 
import { Banner } from './entities/banner.entity';
import { StorageHandler } from './helpers/image-storage';

@Injectable()
export class BannersService {
  constructor(
    @InjectRepository(Banner)
    private bannersRepository: Repository<Banner>
  ) { }

  async create(createBannerDto: CreateBannerDto, user: User): Promise<Banner> {
    const { title, description } = createBannerDto;
    const banner = this.bannersRepository.create({
      title,
      description,
      imagePath: '',
      user
    });

    await this.bannersRepository.save(banner);

    return banner;
  }

  async findAll(userId: string): Promise<Banner[]> {
    if (userId) {
      return await this.bannersRepository.find({ where: { user: +userId } });
    }

    return await this.bannersRepository.find();
  }

  async findOne(id: number): Promise<Banner> {
    const banner = await this.bannersRepository.findOne(id);

    if (!banner) {
      throw new NotFoundException('No banner found');
    }

    return banner;
  }

  async update(id: number, updateBannerDto: UpdateBannerDto): Promise<ResponseBannerDto> {
    const { title, description, imagePath } = updateBannerDto;

    const result = await this.bannersRepository.update(id, { title, description, imagePath })

    if (!result.affected) {
      throw new NotFoundException('This banner does not exist.');
    }
    return { message: 'Update successful', status: 201 };
  }

  async remove(id: number, user: User): Promise<{ message: string, status: number }> {
    const currBanner = user.banners.find(banner => banner.id === id);
    const result = await this.bannersRepository.delete({ id, user });

    if (!result.affected) {
      throw new NotFoundException('Task not found');
    }

    StorageHandler.removeFile(currBanner.imagePath);

    return { message: `Delete is successful`, status: 201 };
  }

  async uploadFile(file: Express.Multer.File, bannerId: string, user: User): Promise<ResponseBannerDto> {
    const fileName = file?.filename;

    if (!fileName) {
      throw new UnsupportedMediaTypeException('File must be a png, jpg or jpeg');
    }
    const path = `/uploads/${user.id}/${file.filename}`;

    const currBanner = user.banners.find(banner => banner.id === +bannerId);
    const oldBannerPath = currBanner.imagePath;
    const updateResult = await this.bannersRepository.update(bannerId, { imagePath: path });

    if (!updateResult.affected) {
      StorageHandler.removeFile(path);
      throw new BadRequestException('Saving file failed');
    }

    if (oldBannerPath) {
      StorageHandler.removeFile(oldBannerPath);
    }

    return { status: 201, message: 'Saved successfully' };
  }
}
