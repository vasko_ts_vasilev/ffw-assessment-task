import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { last } from 'rxjs';
import { Repository } from 'typeorm';

import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>
  ) { }

  async findOne(id: number) {
    const user = await this.usersRepository.findOne({ id });

    if (!user) {
      throw new NotFoundException('User not found');
    }
    // TODO remove password
    return user;
  }

  async update(id: number, updateUserDto: UpdateUserDto): Promise<User> {
    const { firstName, lastName, email } = updateUserDto;

    const user = await this.usersRepository.findOne({ id });

    if (!user) {
      throw new NotFoundException('User not found');
    }

    user['firstName'] = firstName;
    user['lastName'] = lastName;
    user['email'] = email;
    // TODO maybe add password too
    await this.usersRepository.save(user);

    return user;
  }
}
