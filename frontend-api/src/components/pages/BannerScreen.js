import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { Box } from '@mui/material';

import PageTemplate from '../templates/PageTemplate';
import BannerView from '../ui/molecules/BannerView';
import { getBannerById } from './bannersActions';

const BannerScreen = () => {
    const dispatch = useDispatch();
    const { id } = useParams();
    const banner = useSelector(state => state.banners.currBanner);

    useEffect(() => {
        dispatch(getBannerById(id));
    }, []);

    return (
        <PageTemplate>
            <Box
                sx={{
                    bgcolor: 'background.paper',
                    pt: 8,
                    pb: 6
                }}
            >
                <BannerView banner={banner} />
            </Box>
        </PageTemplate>
    );
};

export default BannerScreen;