import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Outlet } from 'react-router-dom';

import PageTemplate from '../templates/PageTemplate';
import PageHeader from '../ui/molecules/PageHeader';
import BannerCardGrid from '../ui/organisms/BannerCardGrid';
import { getAllBanners } from './bannersActions';

const HomeScreen = () => {
    const dispatch = useDispatch();
    const banners = useSelector(state => state.banners.banners);

    useEffect(() => {
        dispatch(getAllBanners(null));
    }, []);

    return (
        <PageTemplate>
            <PageHeader />
            <Outlet />
            <BannerCardGrid  cards={banners} />
        </PageTemplate>
    );
};

export default HomeScreen;