import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import PageTemplate from '../templates/PageTemplate';
import DashboardControls from '../ui/organisms/dashboard/DashboardControls.js';
import BannerCardGrid from '../ui/organisms/BannerCardGrid';
import { getAllBanners, createBanner, updateBanner, deleteBanner } from './bannersActions';

const INITIAL_STATE = {
    id: -1,
    title: '',
    description: '',
    imagePath: null
};

const Dashboard = () => {
    const dispatch = useDispatch();
    const isSignedIn = useSelector(state => state.auth.token);
    const userId = useSelector(state => state.auth.user?.id);
    const banners = useSelector(state => state.banners.banners);
    const [state, setState] = useState(INITIAL_STATE);
    const [blob, setBlob] = useState(null);

    useEffect(() => {
        dispatch(getAllBanners(userId));
    }, []);

    const handleStateChange = ({ target }) => {
        let targetValue = target.value;

        if (target.name === 'imagePath') {
            targetValue = URL.createObjectURL(target.files[0]);
            setBlob(target.files[0]);
        }

        setState({
            ...state,
            [target.name]: targetValue
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        if (!state.imagePath) {
            return;
        }

        if (state.id && state.id !== -1) {
            dispatch(updateBanner(state, blob));
            return;
        };

        dispatch(createBanner(state, blob));
    };

    const handleSelectBanner = ({ target }) => {
        const currBanner = banners.find(banner => banner.id === +target.id);

        setState(currBanner);
        setBlob(null);
    };

    const handleDelete = () => {
        dispatch(deleteBanner(state.id));
    }

    const handleFbShare = (card) => {
        window.FB.ui({
            method: 'share',
            href: `${window.location.origin}/home`,// TODO maybe add some routing for single cards - asked Violina
            quote: card.title
        }, function (_response) { });
    }

    return (
        <PageTemplate>
            <DashboardControls
                state={state}
                handleStateChange={handleStateChange}
                handleSubmit={handleSubmit}
                handleDelete={handleDelete}
            />
            <BannerCardGrid
                cards={banners}
                isSignedIn={isSignedIn}
                handleSelectBanner={handleSelectBanner}
                handleFbShare={handleFbShare}
            />
        </PageTemplate>
    )
};

export default Dashboard;