import {
    CLEAR_STATE,
    SAVE_BANNERS,
    SAVE_CURRENT_BANNER
} from '../../redux-store/reduxTypes';

const INITIAL_STATE = {
    banners: [],
    currBanner: {
        title: null,
        description: null,
        imagePath: null
    }
};

export default (state = INITIAL_STATE, action) => {
    const { type, payload } = action;

    switch (type) {
        case SAVE_BANNERS:
            return {
                ...state,
                banners: payload
            };
        case SAVE_CURRENT_BANNER:
            return {
                ...state,
                currBanner: payload
            };
        case CLEAR_STATE:
            return INITIAL_STATE;
        default:
            return state;
    }
};