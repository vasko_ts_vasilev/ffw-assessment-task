import axios from '../../api/axios';
import { SAVE_BANNERS, SAVE_CURRENT_BANNER } from '../../redux-store/reduxTypes';

export const getBannerById = (id) => async (dispatch) => {
    try {
        const response = await axios.get(`banners/${id}`);

        dispatch({ type: SAVE_CURRENT_BANNER, payload: response.data });
    } catch (err) {
        console.error('ERROR -> ', err);
    }
};

export const getAllBanners = (userId) => async (dispatch, getState) => {
    try {
        let query = '';
        if (userId) {
            query = `?userId=${getState().auth.user.id}`
        }
        const response = await axios.get(`/banners${query}`);

        dispatch({ type: SAVE_BANNERS, payload: response.data });
    } catch (err) {
        console.error(err);
    }
};

export const createBanner = (banner, image) => async (dispatch, getState) => {
    const token = getState().auth.token;

    try {
        const response = await axios.post('/banners', banner, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });

        dispatch(uploadBannerImage(response.data.id, image));
    } catch (err) {
        console.error('Oops.Something went wrong -> ', err);
    }

};

export const updateBanner = (banner, image) => async (dispatch, getState) => {
    if (image) {
        dispatch(uploadBannerImage(banner.id, image));
    }

    try {
        const userId = getState().auth.userId;
        const token = getState().auth.token;
        const response = await axios.patch(`/banners/${banner.id}`, banner, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });

        if (response.data.status === 201 && !image) {
            dispatch(getAllBanners(userId));
        }

    } catch (err) {
        console.error(err);
    }
}

export const deleteBanner = (id) => async (dispatch, getState) => {
    try {
        const userId = getState().auth.user.id;
        const token = getState().auth.token;
        const response = await axios.delete(`/banners/${id}`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });

        if (response.data.status === 201) {
            dispatch(getAllBanners(userId));
        }
    } catch (err) {
        console.error(err);
    }
};

export const uploadBannerImage = (id, image) => async (dispatch, getState) => {
    const userId = getState().auth.user.id;
    const formData = new FormData();
    formData.append('file', image);

    try {
        const response = await axios.post(`/banners/${id}?userId=${userId}`, formData);

        if (response.status === 201) {
            dispatch(getAllBanners(userId));
        }
    } catch (err) {
        console.error('Oops. Something went wrong -> ', err);
    }
}
