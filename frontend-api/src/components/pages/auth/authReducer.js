import { CLEAR_STATE, SAVE_LOGIN_DATA } from '../../../redux-store/reduxTypes';

const INITIAL_STATE = {
    user: null,
    token: null
};

export default (state = INITIAL_STATE, action) => {
    const { type, payload } = action;

    switch (type) {
        case SAVE_LOGIN_DATA:
            return {
                user: payload.user,
                token: payload.token
            };
        case CLEAR_STATE:
            return INITIAL_STATE;
        default:
            return state;
    }
}