import axios from '../../../api/axios';

import { CLEAR_STATE, SAVE_LOGIN_DATA } from '../../../redux-store/reduxTypes';

export const signUp = (user, onSuccessCb) => async (dispatch) => {
    try {
        const response = await axios.post('/auth/sign-up', user);

        dispatch({ type: SAVE_LOGIN_DATA, payload: response.data });

        if (response.data.token) {
            onSuccessCb();
            axios.defaults.headers.Authorization = 'Bearer ' + response.data.token;
        }
    } catch (err) {
        console.error(err);
    }
};

export const signIn = (user, onSuccessCb) => async (dispatch) => {
    try {
        const response = await axios.post('/auth/sign-in', user);

        dispatch({ type: SAVE_LOGIN_DATA, payload: response.data });

        if (response.data.token) {
            onSuccessCb();
            axios.defaults.headers.Authorization = 'Bearer ' + response.data.token;
        }
    } catch (err) {
        console.error(err);

    }
};

export const signOut = (onSuccessCb) => (dispatch) => {
    dispatch({ type: CLEAR_STATE });
    localStorage.clear();

    if (onSuccessCb) {
        onSuccessCb();
    }
}