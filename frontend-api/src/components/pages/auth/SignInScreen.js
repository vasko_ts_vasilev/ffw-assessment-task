import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router';

import AuthTemplate from '../../templates/AuthTemplate';
import Copyright from '../../ui/molecules/Copyright';
import FormHeader from '../../ui/molecules/FormHeader';
import SignInForm from '../../ui/organisms/auth/SignInForm';
import { signIn } from './authActions';

const INITIAL_STATE = {
    email: null,
    password: null
};

const SignInScreen = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [state, setState] = useState(INITIAL_STATE);

    const handleStateChange = ({ target }) => {
        setState({
            ...state,
            [target.name]: target.value
        });
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        const onSuccessCb = () => navigate('/dashboard', { replace: true });

        dispatch(signIn(state, onSuccessCb));
    }

    return (
        <AuthTemplate>
            <FormHeader
                headerText={'Sign In'}
                headerComponent={'h1'}
                headerVariant={'h5'}
            />
            <SignInForm
                handleStateChange={handleStateChange}
                submitForm={handleSubmit} />
            <Copyright sx={{ mt: 8, mb: 4 }} />
        </AuthTemplate>
    );
};

export default SignInScreen;