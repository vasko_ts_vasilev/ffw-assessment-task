import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router';

import AuthTemplate from '../../templates/AuthTemplate';
import FormHeader from '../../ui/molecules/FormHeader';
import SignUpForm from '../../ui/organisms/auth/SignUpForm';
import Copyright from '../../ui/molecules/Copyright';
import { signUp } from './authActions';

const INITIAL_STATE = {
    firstName: null,
    lastName: null,
    email: null,
    password: null
};

const SignUpScreen = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const [state, setState] = useState(INITIAL_STATE);
    
    const handleStateChange = ({ target }) => {
        setState({
            ...state,
            [target.name]: target.value
        });
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        const onSuccessCb = () => navigate('/dashboard', { replace: true });
        
        dispatch(signUp(state, onSuccessCb));
    }

    return (
        <AuthTemplate>
            <FormHeader 
                headerText={'Sign Up'}
                headerComponent={'h1'}
                headerVariant={'h5'}
            />
            <SignUpForm 
                handleStateChange={handleStateChange}
                submitForm={handleSubmit} />
            <Copyright sx={{ mt: 8, mb: 4 }} />
        </AuthTemplate>
    );
};

export default SignUpScreen;