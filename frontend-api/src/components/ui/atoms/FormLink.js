import { Grid } from '@mui/material';
import { Link } from 'react-router-dom';

const FormLink = ({ linkText, linkHref }) => (
    <Grid item xs>
        <Link className='auth-links' to={linkHref}>{linkText}</Link>
    </Grid>
);

export default FormLink;