import { Box, Button, TextField } from '@mui/material';

import FormLinks from '../../molecules/FormLinks';

const FIELDS = [
    {
        autoComplete: 'given-name',
        name: 'firstName',
        label: 'First Name',
        id: 'firstName',
        autoFocus: true
    },
    {
        autoComplete: 'family-name',
        name: 'lastName',
        label: 'Last Name',
        id: 'lastName'
    },
    {
        autoComplete: 'email',
        name: 'email',
        label: 'Email Address',
        id: 'email'
    },
    {
        autoComplete: 'new-password',
        name: 'password',
        label: 'Password',
        id: 'password',
        type: 'password'
    }
];

const LINKS = [
    {
        linkText: 'Already have an account? Sign in',
        linkHref: '/sign-in'
    }
];

const SignUpForm = ({ handleStateChange, submitForm }) => (
    <Box
        component='form'
        sx={{ mt: 3 }}
        onSubmit={submitForm}>

        {
            FIELDS.map((field, fieldIndex) => (
                <TextField
                    key={`signUpForm_${fieldIndex}`}
                    margin='normal'
                    required
                    fullWidth
                    onChange={handleStateChange}
                    {...field}
                />
            ))
        }

        <Button
            type='submit'
            fullWidth
            variant='contained'
            sx={{ mt: 3, mb: 2 }}
        >
            Sign Up
        </Button>

        <FormLinks linksArr={LINKS} />
    </Box>
);

export default SignUpForm;