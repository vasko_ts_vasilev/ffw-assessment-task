import { Box, Button, TextField } from '@mui/material';

import FormLinks from '../../molecules/FormLinks';

const FIELDS = [
    {
        id: 'email',
        label: 'Email Address',
        name: 'email',
        autoComplete: 'email',
        autoFocus: true
    },
    {
        id: 'password',
        label: 'Password',
        name: 'password',
        type: 'password',
        autoComplete: 'current-password'
    }
];

const LINKS = [
    {
        linkText: 'Forgot password?',
        linkHref: '#'
    },
    {
        linkText: 'Don\'t have an account? Sign Up',
        linkHref: '/sign-up'
    }
];

const SignInForm = ({ handleStateChange, submitForm }) => (
    <Box 
        component='form'
        sx={{ mt: 1 }}
        onSubmit={submitForm}>
        {
            FIELDS.map((field, fieldIndex) => (
                <TextField
                    key={`signInForm_${fieldIndex}`}
                    margin='normal'
                    required
                    fullWidth
                    onChange={handleStateChange}
                    {...field}
                />
            ))
        }

        <Button
            type='submit'
            fullWidth
            variant='contained'
            sx={{ mt: 3, mb: 2 }}
        >
            Sign In
        </Button>

        <FormLinks linksArr={LINKS} />
    </Box>
);

export default SignInForm;