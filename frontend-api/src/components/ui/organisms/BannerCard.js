import { Grid, Card, CardMedia, Button } from '@mui/material';
import { Link, useNavigate } from 'react-router-dom';

import FacebookShareBtn from './FacebookShareBtn';
import BannerCardText from '../molecules/BannerCardText';

const BannerCard = ({ card, isSignedIn, handleSelectBanner }) => {
    const navigate = useNavigate();

    const handleClick = isSignedIn ? handleSelectBanner : () => navigate(`/banner/${card.id}`)

    return (
        <Grid item key={card} xs={12} sm={6} md={4}>
            {/* <Link to={`/banner/${card.id}`}> */}
            <Card
                className='banner-card'
                sx={{
                    height: '100%',
                    display: 'flex',
                    flexDirection: 'column',
                    position: 'relative',
                    maxHeight: 330
                }}
            >
                <CardMedia
                    component='img'
                    sx={{
                        height: 330
                    }}
                    image={`http://localhost:8000${card.imagePath}`}
                    alt={card.title}
                />
                <BannerCardText
                    title={card.title}
                    description={card.description}
                />
                <FacebookShareBtn
                    card={card}
                    isSignedIn={isSignedIn}
                />
                <Button
                    id={card.id}
                    variant='outlined' sx={{
                        position: 'absolute',
                        bottom: 6,
                        right: 6,
                        width: 70,
                        zIndex: 120
                    }}
                    onClick={handleClick}>{isSignedIn ? 'Edit' : 'View'}</Button>
            </Card>
            {/* </Link> */}
        </Grid>
    );
}

export default BannerCard;