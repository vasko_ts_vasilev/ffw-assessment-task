import { Box, Container, Typography } from '@mui/material';

import DashboardForm from './DashBoardForm';

const DashboardControls = ({ state, handleStateChange, handleSubmit, handleDelete }) => {

    return (
        <Box
            sx={{
                bgcolor: 'background.paper',
                pt: 8,
                pb: 6
            }}
        >
            <Container maxWidth='sm' sx={{}}>
                {/**TODO
                 * connect to backend services
                 */}
                <Typography
                    component='h1'
                    variant='h2'
                    align='center'
                    color='text.primary'
                    gutterBottom
                >Manage banners</Typography>
                <DashboardForm
                    state={state}
                    handleStateChange={handleStateChange}
                    handleSubmit={handleSubmit}
                    handleDelete={handleDelete}
                />
            </Container>
        </Box>
    );
};

export default DashboardControls;