const UploadImage = ({ imagePath, handleStateChange }) => {
    const imageSource = imagePath && imagePath.includes('blob') ? imagePath : `http://localhost:8000${imagePath}`;

    return (
        <div className='dashboard__uploadImage'>
            <input type='file' accept='image/*' id='imagePath' name='imagePath' onChange={handleStateChange} />
            {
                imagePath && <img src={imageSource} alt='new-file-upload' />
            }
            <div>
                <label htmlFor='imagePath'>Select File...
                    <br />
                    <span>(up to 1MB)</span>
                </label>
            </div>
        </div>
    );
};

export default UploadImage;