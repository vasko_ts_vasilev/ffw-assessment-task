import { Box, TextField, Grid } from '@mui/material';

import UploadImage from './UploadImage';
import SaveDelBtnStack from '../../molecules/SaveDelBtnStack';

const DashboardForm = ({ state, handleStateChange, handleSubmit, handleDelete }) => (
    <Box
        component='form'
        sx={{ mt: 1 }}
        onSubmit={handleSubmit}
    >
        <Grid container spacing={2}>
            <Grid item xs={12} sm={6} md={6} >
                <UploadImage
                    imagePath={state.imagePath}
                    handleStateChange={handleStateChange}
                />
            </Grid>
            <Grid
                item
                xs={12} sm={6} md={6}
                sx={{ display: 'flex', flexDirection: 'column' }}
            >
                <TextField
                    margin='normal'
                    required
                    fullWidth
                    id='title'
                    name='title'
                    label='Title'
                    autoFocus
                    onChange={handleStateChange}
                    value={state.title}
                />
                <TextField
                    margin='normal'
                    required
                    fullWidth
                    id='description'
                    name='description'
                    label='Description'
                    multiline
                    rows={3}
                    onChange={handleStateChange}
                    value={state.description}
                />
                <SaveDelBtnStack itemId={state.id} handleDelete={handleDelete} />
            </Grid>
        </Grid>
    </Box>
);

export default DashboardForm;