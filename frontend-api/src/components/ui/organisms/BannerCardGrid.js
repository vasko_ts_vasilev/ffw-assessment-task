import { Container, Grid } from '@mui/material';

import BannerCard from './BannerCard';

const BannerCardGrid = ({ cards, isSignedIn, handleSelectBanner }) => (
    <Container
        sx={{ py: 8 }}
        maxWidth='md'
    >
        <Grid container spacing={4}>
            {
                cards.map(card => (
                    <BannerCard
                        key={`banner_card_grid_item${card.id}`}
                        card={card}
                        isSignedIn={isSignedIn}
                        handleSelectBanner={handleSelectBanner} />
                ))
            }
        </Grid>
    </Container>
);

export default BannerCardGrid;