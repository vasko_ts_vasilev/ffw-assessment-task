import { Box } from '@mui/material';
import SignOutBtn from '../molecules/SignOutBtn';

const SignedInUser = ({ userFirstName, handleSignOut }) => (
    <Box
        variant='h6'
        component='div'
        sx={{
            display: 'flex',
            flexDirection: 'row'
        }}
    >
        <p>Welcome <strong>{userFirstName}</strong>!</p>
        <SignOutBtn handleSignOut={handleSignOut} />
    </Box>
);

export default SignedInUser;