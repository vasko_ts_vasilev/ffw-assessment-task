import { Button } from '@mui/material';

const FacebookShareBtn = ({ card, isSignedIn }) => {

    const handleFbShare = () => {
        window.FB.ui({
            method: 'share',
            href: `${window.location.origin}/home/${card.id}`,
            quote: card.description
        }, function (_response) { });
    }

    return (
        <Button variant='outlined'
            sx={{
                width: 70,
                position: 'absolute',
                bottom: 6,
                right: 82,
            }} onClick={handleFbShare}>Share</Button>
    );
};
export default FacebookShareBtn;