import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router';
import { Link } from 'react-router-dom';
import { AppBar, Button, Toolbar, Typography } from '@mui/material';

import NavMenu from '../molecules/NavMenu';
import { signOut } from '../../pages/auth/authActions';
import { getAllBanners } from '../../pages/bannersActions';

const Navigation = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const isSignedIn = useSelector(state => state.auth.token);
    const userFirstName = useSelector(state => state.auth.user?.firstName);

    const handleSignOut = () => {
        const onSuccessCb = () => {
            navigate('/home');
            dispatch(getAllBanners(null))
        };

        dispatch(signOut(onSuccessCb));
    };

    return (
        <AppBar position='static'>
            <Toolbar>
                <Typography
                    variant='h6'
                    component='div'
                >
                    LOGO
                </Typography>
                <Link to='/home' style={{ textDecoration: 'none', flexGrow: isSignedIn ? 0 : 1 }}>
                    <Button color='inherit'>
                        Home
                    </Button>
                </Link>
                {
                    isSignedIn ?
                        <NavMenu
                            userFirstName={userFirstName}
                            handleSignOut={handleSignOut}
                        />
                        : <Link
                            to='/sign-in'
                            style={{ textDecoration: 'none', flexGrow: isSignedIn ? 1 : 0 }}
                        >

                            <Button color='inherit'>
                                Login
                            </Button>
                        </Link>
                }
            </Toolbar>
        </AppBar >
    );
};

export default Navigation;