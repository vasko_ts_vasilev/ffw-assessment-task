import { CardContent, Typography } from '@mui/material';

const BannerCardText = ({ title, description }) => (
    <CardContent sx={{
        position: 'absolute',
        bottom: 0,
        width: '100%',
        minHeight: '43%',
        height: '43%',
        backgroundColor: 'rgba(255, 255, 255, 0.73)',
    }}>
        <Typography
            gutterBottom
            variant='h5'
            component='h2'
            sx={{
                display: '-webkit-box',
                textOverflow: 'ellipsis',
                overflow: 'hidden',
                WebkitLineClamp: 1,
                WebkitBoxOrient: 'vertical'
            }}
        >{title}</Typography>
        <Typography
            sx={{
                whiteSpace: 'normal',
                WebkitLineClamp: 2,
                textOverflow: 'ellipsis',
                WebkitBoxOrient: 'vertical',
                overflow: 'hidden',
                display: '-webkit-box',
                marginBottom: '20px'
            }}
        >{description}</Typography>
    </CardContent >
);

export default BannerCardText;