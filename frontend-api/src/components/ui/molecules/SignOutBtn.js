import { IconButton } from '@mui/material';
import { Logout } from '@mui/icons-material';

const SignOutBtn = ({ handleSignOut }) => (
        <IconButton
            size="large"
            aria-label="account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={handleSignOut}
            color="inherit"
        >
            <Logout />
        </IconButton>
);

export default SignOutBtn;