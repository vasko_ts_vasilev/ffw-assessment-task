import { Container, Grid, Typography } from '@mui/material';

const BannerView = ({ banner }) => (
    <Container maxWidth='sm'>
        <Typography
            component='h1'
            variant='h2'
            align='center'
            color='text.primary'
            gutterBottom
        >{banner.title}</Typography>

        <Grid container spacing={2} sx={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center'
        }}>
            <Grid item xs={12} sm={12} md={6}>
                <img
                    src={`http://localhost:8000${banner.imagePath}`}
                    alt={banner.title}
                    style={{
                        width: '100%',
                        height: 330
                    }} />
            </Grid>
            <Grid item xs={12} sm={12} md={6}>
                <Typography component='div'>
                    <Typography sx={{ fontWeight: 700 }}>Short Description</Typography>
                    {banner.description}
                </Typography>
            </Grid>
        </Grid>

    </Container>
);

export default BannerView;