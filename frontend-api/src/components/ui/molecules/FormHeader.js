import { Typography } from '@mui/material';

import FormAvatar from '../atoms/FormAvatar';

const FormHeader = ({ headerText, headerComponent, headerVariant }) => (
    <>
        <FormAvatar />
        <Typography
            component={headerComponent}
            variant={headerVariant}
        >
            {headerText}
        </Typography>
    </>
);

export default FormHeader;