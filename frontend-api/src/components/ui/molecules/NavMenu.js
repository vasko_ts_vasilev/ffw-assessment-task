import { Link } from 'react-router-dom';
import { Button } from '@mui/material';

import SignedInUser from '../organisms/SignedInUser';

const NavMenu = ({ userFirstName, handleSignOut }) => (
    <>
        <div style={{ textDecoration: 'none', display: 'flex', flexGrow: 1 }}>
            <Link to='/dashboard' style={{ textDecoration: 'none' }}>
                <Button color='inherit'>
                    Dashboard
                </Button>
            </Link>
        </div>
        <SignedInUser
            userFirstName={userFirstName}
            handleSignOut={handleSignOut}
        />
    </>
);

export default NavMenu;