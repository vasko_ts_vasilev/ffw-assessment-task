import { Grid } from '@mui/material';

import FormLink from '../atoms/FormLink';

const FormLinks = ({ linksArr }) => (
    <Grid container>
        {
            linksArr.map((formLink, linkIndex) => (
                <FormLink key={`formLink_${linkIndex}`} {...formLink} />
            ))
        }
    </Grid>
);

export default FormLinks;