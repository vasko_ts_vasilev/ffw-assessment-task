import { Stack, Button } from '@mui/material';

const SaveDelBtnStack = ({ itemId, handleDelete }) => (
    <Stack spacing={2} sx={{ flex: 1, justifyContent: 'flex-end' }}>
        <Button variant='outlined' disabled={itemId === -1 ? true : false} onClick={handleDelete}>Delete</Button>
        <Button type='submit' variant='contained'>Save</Button>
    </Stack>
);

export default SaveDelBtnStack;