import { Container, CssBaseline, Box } from '@mui/material';

const AuthTemplate = ({ children }) => (
    <Container component='main' maxWidth='xs'>
        <CssBaseline />
        <Box
            sx={{
                marginTop: 8,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center'
            }}
        >
            {children}
        </Box>
    </Container>
);

export default AuthTemplate;