import { CssBaseline } from '@mui/material';

import CustomFooter from '../ui/organisms/CustomFooter';
import Navigation from '../ui/organisms/Navigation';

const PageTemplate = ({ children }) => (
    <>
        <CssBaseline />
        <Navigation />
        <main>
            {children}
        </main>
        <CustomFooter />
    </>
);

export default PageTemplate;