import { createTheme, ThemeProvider } from '@mui/material/styles';
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate
} from 'react-router-dom';
import { useSelector } from 'react-redux';

import HomeScreen from './components/pages/HomeScreen';
import SignInScreen from './components/pages/auth/SignInScreen';
import SignUpScreen from './components/pages/auth/SignUpScreen';
import Dashboard from './components/pages/Dashboard';
import BannerScreen from './components/pages/BannerScreen';

const theme = createTheme();

function App() {
  const isSignedIn = useSelector(state => state.auth.token);
  const initialRoute = !isSignedIn ? '/home' : '/dashboard';

  return (
    <Router>
      <ThemeProvider theme={theme}>
        <Routes>
          <Route path='/' element={<Navigate replace to={initialRoute} />} />
          <Route path='/home' element={<HomeScreen />} />
          <Route path='/sign-in' element={<SignInScreen />} />
          <Route path='/sign-up' element={<SignUpScreen />} />
          <Route path='/dashboard' element={<Dashboard />} />
          <Route path='/banner/:id' element={<BannerScreen />}/>
        </Routes>
      </ThemeProvider>
    </Router>
  );
}

export default App;
