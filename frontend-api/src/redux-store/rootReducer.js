import { combineReducers } from 'redux';

import authReducer from '../components/pages/auth/authReducer';
import bannersReducer from '../components/pages/bannersReducer';

const rootReducer = combineReducers({
    auth: authReducer,
    banners: bannersReducer,
});

export default rootReducer;