import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { throttle } from 'lodash';

import rootReducer from './rootReducer';

const composedEnhancer = compose(applyMiddleware(thunkMiddleware))

const reHydrateStore = () => {
    if (localStorage.getItem('appState') !== null) {
        return JSON.parse(localStorage.getItem('appState')) // re-hydrate the store
    }
};

const store = createStore(
    rootReducer,
    reHydrateStore(),
    composedEnhancer
);
store.subscribe(throttle(() => {
    localStorage.setItem('appState', JSON.stringify(store.getState()))
}, 1000));

export default store;